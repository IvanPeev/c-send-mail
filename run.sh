#!/bin/bash

echo -ne "1. Gmail\n2. Hotmail\n";
echo -ne "Select your email provider by entering a number: ";
read -r PROVIDER;

if [[ "$PROVIDER" -eq 1 ]]; then
	PROVIDER="smtp://smtp.gmail.com:587";
elif [[ "$PROVIDER" -eq 2 ]]; then
	PROVIDER="smtp://smtp.office365.com:587";
else
	echo "No provider has been selected";
	exit 1;
fi

echo -ne "Enter your email address: ";
read -r FROM;
echo -ne "Enter your password: ";
read -rs PASSWORD;
echo -ne "\nEnter the email you want to send to: ";
read -r TO;
echo -ne "Enter the subject: ";
read -r SUBJECT;
echo -ne "Enter path to a text file to be used as a message: ";
read -r FILE;
MESSAGE=$(<"$FILE");

echo -ne "-----------------------------------------------------------\n";

echo "From:" "$FROM";
echo "To:" "$TO";
echo "Subject:" "$SUBJECT";
echo -ne "\n" "$MESSAGE" "\n\n";

echo -ne "-----------------------------------------------------------\n";

echo -ne "Are you sure you want to send this mail? (yes/no) ? ";
read -r CHOICE;

if [ "$CHOICE" = "yes" ]; then
	file=./main

	if [ -e "$file" ]; then
		rm ./main;
	fi

	gcc -o main main.c -g -lcurl;
	./main "$FROM" "$PASSWORD" "$TO" "$SUBJECT" "$MESSAGE" "$PROVIDER";
else
	echo "Goodbye!";
	exit 0;
fi
