#include <curl/curl.h>
#include <string.h>
#include <stdio.h>

char *from;
char *password;
char *to;
char *cc;
char *subject;
char *message;

struct upload_status {
	int lines_read;
};

void setFrom(char* arg) {
	from = arg;
}

void setPassword(char* arg) {
	password = arg;
}

void setTo(char* arg) {
	to = arg;
}

void setCC(char* arg) {
	cc = arg;
}

void setSubject(char* arg) {
	subject = arg;
}

void setMessage(char* arg) {
	message = arg;
}

static size_t payload_source(void *ptr, size_t size, size_t nmemb, void *userp) {
	struct upload_status *upload_ctx = (struct upload_status *) userp;
	const char *data;

	if ((size == 0) || (nmemb == 0) || ((size * nmemb) < 1)) {
		return 0;
	}

	char *payload_text[] = {
		"To: ", to, "\r\n",
		"From: ", from ,"\r\n",
		// "Cc: ", cc, "\r\n",
		"Subject:", subject, " \r\n",
		"\r\n",
		message,
		NULL
	};

	data = payload_text[upload_ctx->lines_read];

	if (data) {
		size_t len = strlen(data);
		memcpy(ptr, data, len);
		upload_ctx->lines_read++;

		return len;
	}

	return 0;
}

int main(int argc, char *argv[]) {
	CURL *curl;
	CURLcode res = CURLE_OK;
	struct curl_slist *recipients = NULL;
	struct upload_status upload_ctx;

	setFrom(argv[1]);
	setPassword(argv[2]);
	setTo(argv[3]);
	setSubject(argv[4]);
	setMessage(argv[5]);
	// setCC("");

	upload_ctx.lines_read = 0;
	curl = curl_easy_init();

	if (curl) {
		curl_easy_setopt(curl, CURLOPT_USERNAME, from);
		curl_easy_setopt(curl, CURLOPT_PASSWORD, password);

		curl_easy_setopt(curl, CURLOPT_URL, argv[6]);

		curl_easy_setopt(curl, CURLOPT_USE_SSL, (long) CURLUSESSL_ALL);

		// curl_easy_setopt(curl, CURLOPT_CAINFO, "/path/to/certificate.pem");

		curl_easy_setopt(curl, CURLOPT_MAIL_FROM, from);

		recipients = curl_slist_append(recipients, to);
		// recipients = curl_slist_append(recipients, cc);
		curl_easy_setopt(curl, CURLOPT_MAIL_RCPT, recipients);

		curl_easy_setopt(curl, CURLOPT_READFUNCTION, payload_source);
		curl_easy_setopt(curl, CURLOPT_READDATA, &upload_ctx);
		curl_easy_setopt(curl, CURLOPT_UPLOAD, 1L);

		curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);

		res = curl_easy_perform(curl);

		if (res != CURLE_OK) {
			fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
		}

		curl_slist_free_all(recipients);
		curl_easy_cleanup(curl);
		printf("Mail sent!\n");
	}

	return (int) res;
}
